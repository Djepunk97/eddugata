-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2018 at 07:14 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id4028711_user`
--

-- --------------------------------------------------------

--
-- Table structure for table `hub`
--

CREATE TABLE `hub` (
  `id_user` int(11) NOT NULL,
  `nama_lkp` varchar(50) NOT NULL,
  `tmpt_lhr` varchar(50) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jk` enum('Laki-laki','Perempuan') NOT NULL,
  `telp` int(13) NOT NULL,
  `email` varchar(30) NOT NULL,
  `asal_sklh` varchar(30) NOT NULL,
  `alamat_sklh` varchar(30) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `skill` longtext NOT NULL,
  `kontak_instansi` int(13) NOT NULL,
  `minat` varchar(100) NOT NULL,
  `mulai` date NOT NULL,
  `akhir` date NOT NULL,
  `drmn` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hub`
--

INSERT INTO `hub` (`id_user`, `nama_lkp`, `tmpt_lhr`, `tgl_lhr`, `alamat`, `jk`, `telp`, `email`, `asal_sklh`, `alamat_sklh`, `jurusan`, `skill`, `kontak_instansi`, `minat`, `mulai`, `akhir`, `drmn`) VALUES
(1, 'Naufal Andrianto Nurfauzi', 'Malang', '1970-01-01', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'adsfjadsfiuahidshfuaydufhuiadfiyad7f', 2147483647, 'Backend', '2018-02-01', '2018-02-02', 'dari sana'),
(2, 'Muhammad Rohan', 'Palu', '1945-02-02', 'Ujung Kulon', 'Laki-laki', 2147483647, 'fadfaffadf@gmail.com', 'SMKN 100 PALU', 'JL aja gak jadian', 'RPL', 'PHP --&gt;10', 2147483647, 'Backend', '2018-02-02', '2018-02-05', 'dari sebelah'),
(3, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'akdjf', '1901-05-06', 'dafdaf', 'Perempuan', 2147483647, 'pekuvo@crypemail.info', 'adfadf', 'hfjhfhgj', 'sdfghdgh', 'sdfhsgfhgfhsfgergs', 2147483647, 'dfkhahsdfhasdgfgadgfygaf', '2020-08-08', '2045-08-12', 'dfadfadfaefawefwadsf'),
(4, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'akdjf', '1901-05-06', 'dafdaf', 'Perempuan', 2147483647, 'pekuvo@crypemail.info', 'adfadf', 'hfjhfhgj', 'sdfghdgh', 'asdfadfadf', 2147483647, 'adfafefaewfads', '2020-08-08', '2045-08-12', 'dfadfadfaefawefwadsf'),
(5, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'akdjf', '1901-05-06', 'dafdaf', 'Perempuan', 2147483647, 'pekuvo@crypemail.info', 'adfadf', 'hfjhfhgj', 'sdfghdgh', 'asdfertwyhben', 2147483647, 'agshjhkiuytrew', '2020-08-08', '2045-08-12', 'dfadfadfaefawefwadsf'),
(6, 'tes', 'tes', '1992-01-01', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-26', '2018-02-26', 'tes'),
(7, 'tes', 'tes', '2018-01-26', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'adsfasdf', 2147483647, 'tes', '2018-01-26', '2018-02-26', 'tes'),
(8, 'genah', 'genah', '2018-01-26', 'genah', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'genah', 'genah', 'genah', 'genah', 2147483647, 'genah', '2018-01-26', '2018-03-26', 'dari pojokam'),
(9, 'siap', 'siap', '2018-01-26', 'siap', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'siap', 'siap', 'siap', 'adf', 0, 'suiap', '2018-01-26', '2018-04-26', 'adf'),
(10, 'genah', 'Palu', '2018-01-26', 'genah', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'SMKN 100 PALU', 'JL. Gajayana 50', 'RPL', 'asdfadsfadsfa', 0, 'adfadsfasdf', '2018-02-08', '2018-02-01', 'afdfa'),
(11, 'fadjf', 'akdjf', '2018-01-08', 'Pasuruan', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'adf', 2147483647, 'tes', '2018-02-10', '2018-03-02', 'aaaa'),
(12, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(13, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(14, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(15, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(16, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'Palu', '2018-02-01', 'dafdaf', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'genah', 'tes', 'sdfghdgh', 'adfa', 2147483647, 'fff', '2018-02-09', '2018-03-23', 'fffffff'),
(17, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'Palu', '2018-02-01', 'dafdaf', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'genah', 'tes', 'sdfghdgh', 'adfa', 2147483647, 'fff', '2018-02-09', '2018-03-23', 'fffffff'),
(18, 'Naufal Andrianto Nurfauzi', 'Palu', '2018-01-13', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'tes', 'sdfghdgh', 'ffffffff', 2147483647, 'ffffffffffffff', '2018-02-03', '2018-06-06', 'ffffffffff'),
(19, 'adf', 'af', '2018-01-03', 'dafdaf', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'JL. Gajayana 50', 'Teknik Informatika', 'aaaaaaaaaaaaaaaa', 2147483647, 'aaaaaaaaaaaaa', '2018-02-09', '2018-02-10', 'aaaaa'),
(20, 'fadjfjadhfjha', 'akdjf', '2018-01-08', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'tes', 'Teknik Informatika', 'sdfg', 2147483647, 'sdfgh', '2018-02-08', '2018-02-08', 'xcvbbbbbn'),
(21, 'fadjfjadhfjhadfkjadhfah', 'genah', '2018-01-04', 'genah', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'RPL', 'adfs', 2147483647, 'aaaaaaaaaa', '2018-02-10', '2018-02-10', 'aaaaaaaaa'),
(22, 'tes', 'genah', '2018-01-27', 'tes', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'RPL', 'aaaaaaaaaa', 2147483647, 'aaaaaaaaaaaa', '2018-02-03', '2018-02-10', 'aaaaaaaaa'),
(23, 'Naufal Andr', 'Palu', '2018-01-15', 'dafdaf', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'JL. Gajayana 50', 'genah', 'xfchjsthg', 2147483647, 'wertdyfghj', '2018-02-10', '2018-03-23', 'edfghjk'),
(24, 'abcd', 'abcd', '2018-01-16', 'abcd', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'okke', 'abcd', 'abcd', 'abcd', 2147483647, 'abcd', '2018-02-09', '2018-02-10', 'abcd'),
(25, 'def', 'def', '2018-01-13', 'def', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'def', 'def', 'def', 'def', 2147483647, 'def', '2016-03-03', '2018-01-29', 'def'),
(26, 'jkl', 'jkl', '2017-01-04', 'jkl', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'jkl', 'jkl', 'jkl', 'jkl', 2147483647, 'jkl', '2018-02-10', '2018-03-10', 'jkl'),
(27, 'bismillah', 'bismillah', '2018-02-09', 'bismillah', 'Laki-laki', 2147483647, 'pekuvo@crypemail.info', 'bismillah', 'bismillah', 'bismillah', 'bismillah', 2147483647, 'bismillah', '2018-02-08', '2018-02-10', 'bismillah'),
(28, 'tass', 'tass', '2018-02-10', 'tass', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tass', 'tass', 'tass', 'tass', 2147483647, 'tass', '2018-02-10', '2018-03-10', 'tass'),
(29, 'tass', 'tass', '2018-02-10', 'tass', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tass', 'tass', 'tass', 'tass', 2147483647, 'tass', '2018-02-10', '2018-03-10', 'tass'),
(30, 'tesa', 'tesa', '2018-01-12', 'tesa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'JL aja gak jadian', 'genah', 'asdf', 2147483647, 'tjk', '2018-02-08', '2018-02-10', 'pouiu'),
(31, 'gg', 'gg', '2018-02-09', 'gg', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'gg', 'gg', 'gg', 'gg', 2147483647, 'gg', '2018-02-10', '2018-03-09', 'gg'),
(32, 'gg', 'gg', '2018-02-09', 'gg', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'gg', 'gg', 'gg', 'gg', 2147483647, 'gg', '2018-02-10', '2018-03-09', 'gg'),
(33, 'gg', 'gg', '2018-02-09', 'gg', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'gg', 'gg', 'gg', 'gg', 2147483647, 'gg', '2018-02-10', '2018-03-09', 'gg'),
(34, 'genah', 'akdjf', '2018-01-12', 'tes', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'sdfghdgh', 'ewghfgj', 2147483647, 'weetstdgfhj', '2018-02-07', '2018-02-08', 'qwerstdgfh'),
(35, 'genah', 'akdjf', '2018-01-12', 'tes', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'sdfghdgh', 'ewghfgj', 2147483647, 'weetstdgfhj', '2018-02-07', '2018-02-08', 'qwerstdgfh'),
(36, 'adfadf', 'adfadf', '2018-01-26', 'aaaaaa', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'JL. Gajayana 50', 'sdfghdgh', 'jkl', 2147483647, 'adf', '2018-01-31', '2018-02-01', 'n'),
(37, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(38, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(39, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(40, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(41, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-31', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-24', '2018-02-14', 'tes'),
(42, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-31', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-24', '2018-02-14', 'tes'),
(43, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-09', 'Pasuruan', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'testes', 'tes', 2147483647, 'tes', '2018-02-02', '2018-02-10', 'tes'),
(44, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-30', 'Pasuruan', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'a', 2147483647, 'a', '2018-01-31', '2018-02-10', 'a'),
(45, 'tes', 'tes', '2018-01-24', 'tes', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-31', '2018-01-24', 'tes'),
(46, 'tes', 'tes', '2018-01-31', 'tes', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-31', '2018-01-31', 'tes'),
(47, 'tes', 'tes', '2018-01-31', 'tes', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-31', '2018-01-31', 'tes');

-- --------------------------------------------------------

--
-- Table structure for table `internship`
--

CREATE TABLE `internship` (
  `id_user` int(11) NOT NULL,
  `nama_lkp` varchar(50) NOT NULL,
  `tmpt_lhr` varchar(50) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jk` enum('Laki-laki','Perempuan') NOT NULL,
  `telp` int(13) NOT NULL,
  `email` varchar(30) NOT NULL,
  `asal_sklh` varchar(30) NOT NULL,
  `alamat_sklh` varchar(30) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `skill` longtext NOT NULL,
  `kontak_instansi` int(13) NOT NULL,
  `minat` varchar(100) NOT NULL,
  `mulai` date NOT NULL,
  `akhir` date NOT NULL,
  `drmn` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship`
--

INSERT INTO `internship` (`id_user`, `nama_lkp`, `tmpt_lhr`, `tgl_lhr`, `alamat`, `jk`, `telp`, `email`, `asal_sklh`, `alamat_sklh`, `jurusan`, `skill`, `kontak_instansi`, `minat`, `mulai`, `akhir`, `drmn`) VALUES
(1, 'Naufal Andrianto Nurfauzi', 'Malang', '1970-01-01', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'adsfjadsfiuahidshfuaydufhuiadfiyad7f', 2147483647, 'Backend', '2018-02-01', '2018-02-02', 'dari sana'),
(2, 'Muhammad Rohan', 'Palu', '1945-02-02', 'Ujung Kulon', 'Laki-laki', 2147483647, 'fadfaffadf@gmail.com', 'SMKN 100 PALU', 'JL aja gak jadian', 'RPL', 'PHP --&gt;10', 2147483647, 'Backend', '2018-02-02', '2018-02-05', 'dari sebelah'),
(3, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'akdjf', '1901-05-06', 'dafdaf', 'Perempuan', 2147483647, 'pekuvo@crypemail.info', 'adfadf', 'hfjhfhgj', 'sdfghdgh', 'sdfhsgfhgfhsfgergs', 2147483647, 'dfkhahsdfhasdgfgadgfygaf', '2020-08-08', '2045-08-12', 'dfadfadfaefawefwadsf'),
(4, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'akdjf', '1901-05-06', 'dafdaf', 'Perempuan', 2147483647, 'pekuvo@crypemail.info', 'adfadf', 'hfjhfhgj', 'sdfghdgh', 'asdfadfadf', 2147483647, 'adfafefaewfads', '2020-08-08', '2045-08-12', 'dfadfadfaefawefwadsf'),
(5, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'akdjf', '1901-05-06', 'dafdaf', 'Perempuan', 2147483647, 'pekuvo@crypemail.info', 'adfadf', 'hfjhfhgj', 'sdfghdgh', 'asdfertwyhben', 2147483647, 'agshjhkiuytrew', '2020-08-08', '2045-08-12', 'dfadfadfaefawefwadsf'),
(6, 'tes', 'tes', '1992-01-01', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-26', '2018-02-26', 'tes'),
(7, 'tes', 'tes', '2018-01-26', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'adsfasdf', 2147483647, 'tes', '2018-01-26', '2018-02-26', 'tes'),
(8, 'genah', 'genah', '2018-01-26', 'genah', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'genah', 'genah', 'genah', 'genah', 2147483647, 'genah', '2018-01-26', '2018-03-26', 'dari pojokam'),
(9, 'siap', 'siap', '2018-01-26', 'siap', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'siap', 'siap', 'siap', 'adf', 0, 'suiap', '2018-01-26', '2018-04-26', 'adf'),
(10, 'genah', 'Palu', '2018-01-26', 'genah', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'SMKN 100 PALU', 'JL. Gajayana 50', 'RPL', 'asdfadsfadsfa', 0, 'adfadsfasdf', '2018-02-08', '2018-02-01', 'afdfa'),
(11, 'fadjf', 'akdjf', '2018-01-08', 'Pasuruan', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'adf', 2147483647, 'tes', '2018-02-10', '2018-03-02', 'aaaa'),
(12, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(13, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(14, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(15, 'adsfa', 'saf', '2018-01-06', 'aaaa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'sdfghdgh', 'adsfadf', 0, 'asdfadf', '2018-01-31', '2018-03-21', 'ffff'),
(16, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'Palu', '2018-02-01', 'dafdaf', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'genah', 'tes', 'sdfghdgh', 'adfa', 2147483647, 'fff', '2018-02-09', '2018-03-23', 'fffffff'),
(17, 'fadjfjadhfjhadfkjadhfahdfakdfkjaskjfs', 'Palu', '2018-02-01', 'dafdaf', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'genah', 'tes', 'sdfghdgh', 'adfa', 2147483647, 'fff', '2018-02-09', '2018-03-23', 'fffffff'),
(18, 'Naufal Andrianto Nurfauzi', 'Palu', '2018-01-13', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'tes', 'sdfghdgh', 'ffffffff', 2147483647, 'ffffffffffffff', '2018-02-03', '2018-06-06', 'ffffffffff'),
(19, 'adf', 'af', '2018-01-03', 'dafdaf', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'JL. Gajayana 50', 'Teknik Informatika', 'aaaaaaaaaaaaaaaa', 2147483647, 'aaaaaaaaaaaaa', '2018-02-09', '2018-02-10', 'aaaaa'),
(20, 'fadjfjadhfjha', 'akdjf', '2018-01-08', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'tes', 'Teknik Informatika', 'sdfg', 2147483647, 'sdfgh', '2018-02-08', '2018-02-08', 'xcvbbbbbn'),
(21, 'fadjfjadhfjhadfkjadhfah', 'genah', '2018-01-04', 'genah', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'RPL', 'adfs', 2147483647, 'aaaaaaaaaa', '2018-02-10', '2018-02-10', 'aaaaaaaaa'),
(22, 'tes', 'genah', '2018-01-27', 'tes', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'tes', 'RPL', 'aaaaaaaaaa', 2147483647, 'aaaaaaaaaaaa', '2018-02-03', '2018-02-10', 'aaaaaaaaa'),
(23, 'Naufal Andr', 'Palu', '2018-01-15', 'dafdaf', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'JL. Gajayana 50', 'genah', 'xfchjsthg', 2147483647, 'wertdyfghj', '2018-02-10', '2018-03-23', 'edfghjk'),
(24, 'abcd', 'abcd', '2018-01-16', 'abcd', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'okke', 'abcd', 'abcd', 'abcd', 2147483647, 'abcd', '2018-02-09', '2018-02-10', 'abcd'),
(25, 'def', 'def', '2018-01-13', 'def', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'def', 'def', 'def', 'def', 2147483647, 'def', '2016-03-03', '2018-01-29', 'def'),
(26, 'jkl', 'jkl', '2017-01-04', 'jkl', 'Perempuan', 2147483647, 'naufalnurfauzi@gmail.com', 'jkl', 'jkl', 'jkl', 'jkl', 2147483647, 'jkl', '2018-02-10', '2018-03-10', 'jkl'),
(27, 'bismillah', 'bismillah', '2018-02-09', 'bismillah', 'Laki-laki', 2147483647, 'pekuvo@crypemail.info', 'bismillah', 'bismillah', 'bismillah', 'bismillah', 2147483647, 'bismillah', '2018-02-08', '2018-02-10', 'bismillah'),
(28, 'tass', 'tass', '2018-02-10', 'tass', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tass', 'tass', 'tass', 'tass', 2147483647, 'tass', '2018-02-10', '2018-03-10', 'tass'),
(29, 'tass', 'tass', '2018-02-10', 'tass', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tass', 'tass', 'tass', 'tass', 2147483647, 'tass', '2018-02-10', '2018-03-10', 'tass'),
(30, 'tesa', 'tesa', '2018-01-12', 'tesa', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'adfadf', 'JL aja gak jadian', 'genah', 'asdf', 2147483647, 'tjk', '2018-02-08', '2018-02-10', 'pouiu'),
(31, 'gg', 'gg', '2018-02-09', 'gg', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'gg', 'gg', 'gg', 'gg', 2147483647, 'gg', '2018-02-10', '2018-03-09', 'gg'),
(32, 'gg', 'gg', '2018-02-09', 'gg', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'gg', 'gg', 'gg', 'gg', 2147483647, 'gg', '2018-02-10', '2018-03-09', 'gg'),
(33, 'gg', 'gg', '2018-02-09', 'gg', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'gg', 'gg', 'gg', 'gg', 2147483647, 'gg', '2018-02-10', '2018-03-09', 'gg'),
(34, 'genah', 'akdjf', '2018-01-12', 'tes', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'sdfghdgh', 'ewghfgj', 2147483647, 'weetstdgfhj', '2018-02-07', '2018-02-08', 'qwerstdgfh'),
(35, 'genah', 'akdjf', '2018-01-12', 'tes', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'sdfghdgh', 'ewghfgj', 2147483647, 'weetstdgfhj', '2018-02-07', '2018-02-08', 'qwerstdgfh'),
(36, 'adfadf', 'adfadf', '2018-01-26', 'aaaaaa', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'JL. Gajayana 50', 'sdfghdgh', 'jkl', 2147483647, 'adf', '2018-01-31', '2018-02-01', 'n'),
(37, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(38, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(39, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(40, 'Naufal Andrianto Nurfauzi', 'Malang', '1997-08-28', 'Pasuruan', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'tes', 2147483647, 'tes', '2018-01-31', '2018-02-28', 'tes'),
(41, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-31', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-24', '2018-02-14', 'tes'),
(42, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-31', 'tes', 'Laki-laki', 2147483647, 'naufalnurfauzi@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-24', '2018-02-14', 'tes'),
(43, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-09', 'Pasuruan', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'testes', 'tes', 2147483647, 'tes', '2018-02-02', '2018-02-10', 'tes'),
(44, 'Naufal Andrianto Nurfauzi', 'Malang', '2018-01-30', 'Pasuruan', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'UIN MALANG', 'JL. Gajayana 50', 'Teknik Informatika', 'a', 2147483647, 'a', '2018-01-31', '2018-02-10', 'a'),
(45, 'tes', 'tes', '2018-01-24', 'tes', 'Laki-laki', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-31', '2018-01-24', 'tes'),
(46, 'tes', 'tes', '2018-01-31', 'tes', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-31', '2018-01-31', 'tes'),
(47, 'tes', 'tes', '2018-01-31', 'tes', 'Perempuan', 2147483647, 'naufal.gaoel87@gmail.com', 'tes', 'tes', 'tes', 'tes', 2147483647, 'tes', '2018-01-31', '2018-01-31', 'tes');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(16) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `kelas` varchar(30) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `username`, `email`, `password`, `kelas`, `status`) VALUES
(1, 'naufal', 'djepunk97', 'naufal.gaoel87@gmail.com', '978f4175720b218556b0beca69e2124b', 'Android De', 0),
(2, 'naufal', 'djepunk97', 'naufal.gaoel87@gmail.com', '978f4175720b218556b0beca69e2124b', 'Android Developer', 0),
(3, 'adsfasdf', 'fadsfadsf', 'naufal.gaoel87@gmail.com', '978f4175720b218556b0beca69e2124b', 'Web Developer', 0),
(4, 'adsfasdf', 'fadsfadsf', 'naufal.gaoel87@gmail.com', '978f4175720b218556b0beca69e2124b', 'Web Developer', 0),
(5, 'hahahah', 'wahahahaha', 'naufal.gaoel87@gmail.com', '978f4175720b218556b0beca69e2124b', 'Web Developer', 0),
(6, 'wkwkwkw', 'wth', 'naufal.gaoel87@gmail.com', '978f4175720b218556b0beca69e2124b', 'Android Developer', 0),
(7, 'wkwkwkw', 'waaaaaaaaaaaaaaa', 'naufal.gaoel87@gmail.com', '6dae46591192a45eedecbb203b612700', 'Android Developer', 0),
(8, 'opo', 'seh', 'naufal.gaoel87@gmail.com', '978f4175720b218556b0beca69e2124b', 'Web Developer', 0),
(9, 'wihihi', 'haw', 'naufal.gaoel87@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'Android Developer', 0),
(10, 'dfadfa', 'adsfadf', 'naufal.gaoel87@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'Web Developer', 0),
(11, 'adsfadsfadf', 'uhuhhu', 'naufal.gaoel87@gmail.com', '482c811da5d5b4bc6d497ffa98491e38', 'Android Developer', 0),
(12, 'adfadf', 'adsfadf', 'naufal.gaoel87@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'Android Developer', 0),
(13, 'naufal', 'djepunk97', 'nepuzofid@ugimail.net', '6923af09161dec7c5fd3d6a6b3b4f8b3', 'Android Developer', 0),
(18, 'naufal', 'djepunk97', 'kijoki@20boxme.org', '978f4175720b218556b0beca69e2124b', 'Web Developer', 1),
(19, 'klklklklklk', 'pocong', 'woyodap@send22u.info', '25d55ad283aa400af464c76d713c07ad', 'Android Developer', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hub`
--
ALTER TABLE `hub`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `internship`
--
ALTER TABLE `internship`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hub`
--
ALTER TABLE `hub`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `internship`
--
ALTER TABLE `internship`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
