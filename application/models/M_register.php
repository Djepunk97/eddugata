<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_register extends CI_Model{

  public function insert_user_internship($data){
    $this->db->insert('internship',$data);
    return $this->db->insert_id();
  }

  public function insert_user_hub($data){
    $this->db->insert('hub',$data);
    return $this->db->insert_id();
  }
}
