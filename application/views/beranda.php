<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
 ?>

 <!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
           <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

          <title>Edugata Course an Edu</title>

          <!-- Google font -->
          <link href="https://fonts.googleapis.com/css?family=Lato:700%7CMontserrat:400,600" rel="stylesheet">

          <!-- Bootstrap -->
          <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>"/>

          <!-- Font Awesome Icon -->
          <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">

          <!-- Custom stlylesheet -->
          <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>"/>

          <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/animate.css')?>"/>

          <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
          <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
          <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

    </head>
     <body>

          <!-- Header -->
          <header id="header" class="transparent-nav">
               <div class="container">

                    <div class="navbar-header">
                         <!-- Logo -->
                         <div class="navbar-brand">
                              <a class="logo" href="index.html">
                                   <img src="<?php echo base_url('assets/img/logo11.png')?>" alt="logo">
                              </a>
                         </div>
                         <!-- /Logo -->

                         <!-- Mobile toggle -->
                         <button class="navbar-toggle">
                              <span></span>
                         </button>
                         <!-- /Mobile toggle -->
                    </div>

                    <!-- Navigation -->
                    <nav id="nav">
                         <ul class="main-menu nav navbar-nav navbar-right">
                              <li><a href="<?php echo base_url() ?>">Home</a></li>
                              <li><a href="#learnmore">Course</a></li>
                              <li><a href="#learnmore">Pricing</a></li>
                              <!-- <li><a href="<?php echo base_url('register_hub') ?>">Inagata Hub</a></li>
                              <li><a href="blog.html">Inagata Class</a></li>
                              <li><a href="<?php echo base_url('register_internship') ?>">Inagata Internship</a></li> -->
                         </ul>
                    </nav>
                    <!-- /Navigation -->

               </div>
          </header>
          <!-- /Header -->

          <!-- Home -->
          <div id="home" class="hero-area">

               <!-- Backgound Image -->
               <div class="bg-image bg-parallax overlay" style="background-image:url(<?php echo base_url('assets/img/home-background.jpg')?>)"></div>
               <!-- /Backgound Image -->

               <div class="home-wrapper">
                    <div class="container">
                         <div class="row">
                              <div class="col-md-8">
                                   <h1 class="white-text">Course an Edu</h1>
                                   <!-- <a href="#learnmore"><button type="button" class="large button">Course</button></a> -->
                                   <p class="lead white-text">Belajar tanpa batas menembus ruang impian.</p>
                                   <!-- <a class="main-button icon-button" href="#">Get Started!</a> -->
                              </div>
                         </div>
                    </div>
               </div>

          </div>
          <!-- /Home -->

          <!-- About -->
          <div id="about" class="section">

               <!-- container -->
               <div class="container">

                    <!-- row -->
                    <div class="row">

                         <div class="col-md-6">
                              <div class="section-header">
                                   <h2>Welcome to Edusite</h2>
                                   <p class="lead">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
                              </div>

                              <!-- feature -->
                              <div class="feature">
                                   <i class="feature-icon fa fa-flask"></i>
                                   <div class="feature-content">
                                        <h4>Online Courses </h4>
                                        <p>Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                                   </div>
                              </div>
                              <!-- /feature -->

                              <!-- feature -->
                              <div class="feature">
                                   <i class="feature-icon fa fa-users"></i>
                                   <div class="feature-content">
                                        <h4>Expert Teachers</h4>
                                        <p>Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                                   </div>
                              </div>
                              <!-- /feature -->

                              <!-- feature -->
                              <div class="feature">
                                   <i class="feature-icon fa fa-comments"></i>
                                   <div class="feature-content">
                                        <h4>Community</h4>
                                        <p>Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                                   </div>
                              </div>
                              <!-- /feature -->

                         </div>

                         <div class="col-md-6">
                              <div class="about-img">
                                   <img src="<?php echo base_url('assets/img/about.png') ?>" alt="">
                              </div>
                         </div>

                    </div>
                    <!-- row -->

               </div>
               <!-- container -->
          </div>
          <!-- /About -->

          <!-- Courses -->
          <div id="courses" class="section">

               <!-- container -->
               <div class="container">

                    <!-- row -->
                    <div class="row">
                         
                         <div class="section-header text-center">
                              
                              <a class="anchor animated bounce" name="learnmore"></a>

                              <h2>Explore Courses</h2>
                              <p class="lead">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
                         </div>
                         
                    </div>
                    <!-- /row -->

                    <!-- courses -->
                    <div id="courses-wrapper">

                         <!-- row -->
                         <div class="row">
                              <!-- <a class="anchor" name="learnmore"></a> -->
                              <!-- single course -->
                              <div class="col-md-4 col-sm-12 col-xs-12">
                                   <div class="course">
                                        <a href="<?php echo base_url('register_internship') ?>" class="course-img">
                                             <img src="<?php echo base_url('assets/img/course05.jpg') ?>" alt="">
                                             <i class="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a class="course-title" href="<?php echo base_url('register_internship') ?>">Inagata Internship</a>
                                        <div class="course-details">
                                             <span class="course-category"></span>
                                             <span class="course-price course-free"></span>
                                        </div>
                                   </div>
                              </div>
                              <!-- /single course -->

                              <!-- single course -->
                              <div class="col-md-4 col-sm-12 col-xs-12">
                                   <div class="course">
                                        <a href="<?php echo base_url('register_hub') ?>" class="course-img">
                                             <img src="<?php echo base_url('assets/img/course02.jpg')?>" alt="">
                                             <i class="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a class="course-title" href="<?php echo base_url('register_hub') ?>">Inagata Hub</a>
                                        <div class="course-details">
                                             <span class="course-category"></span>
                                             <span class="course-price course-premium"></span>
                                        </div>
                                   </div>
                              </div>
                              <!-- /single course -->

                              <!-- single course -->
                              <div class="col-md-4 col-sm-12 col-xs-12">
                                   <div class="course">
                                        <a href="#" class="course-img">
                                             <img src="<?php echo base_url('assets/img/course06.jpg')?>" alt="">
                                             <i class="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a class="course-title" href="#">Inagata Class</a>
                                        <div class="course-details">
                                             <span class="course-category"></span>
                                             <span class="course-price course-free"></span>
                                        </div>
                                   </div>
                              </div>
                              <!-- /single course -->

                              <!-- <div class="col-md-3 col-sm-6 col-xs-6">
                                   <div class="course">
                                        <a href="#" class="course-img">
                                             <img src="<?php echo base_url('assets/img/course04.jpg') ?> " alt="">
                                             <i class="course-link-icon fa fa-link"></i>
                                        </a>
                                        <a class="course-title" href="#">The Complete Web Development Course</a>
                                        <div class="course-details">
                                             <span class="course-category">Web Development</span>
                                             <span class="course-price course-free">Free</span>
                                        </div>
                                   </div>
                              </div> -->
                              <!-- /single course -->

                         </div>
                         <!-- /row -->

                         <!-- row -->
                         <div class="row">

                              <!-- single course -->
                              <div class="col-md-3 col-sm-6 col-xs-6">
                                   
                              </div>
                              <!-- /single course -->

                              <!-- single course -->
                              <div class="col-md-3 col-sm-6 col-xs-6">
                                   
                              </div>
                              <!-- /single course -->

                              <!-- single course -->
                              
                              <!-- /single course -->


                              <!-- single course -->
          
                              <!-- /single course -->

                         </div>
                         <!-- /row -->

                    </div>
                    <!-- /courses -->

                    <div class="row">
                         <div class="center-btn">
                              <a class="main-button icon-button" href="#">More Courses</a>
                         </div>
                    </div>

               </div>
               <!-- container -->

          </div>
          <!-- /Courses -->

          <!-- Call To Action -->
          <div id="cta" class="section">

               <!-- Backgound Image -->
               <div class="bg-image bg-parallax overlay" style="background-image:url(<?php echo base_url('assets/img/cta1-background.jpg');?>)"></div>
               <!-- /Backgound Image -->

               <!-- container -->
               <div class="container">

                    <!-- row -->
                    <div class="row">

                         <div class="col-md-6">
                              <h2 class="white-text">Ceteros fuisset mei no, soleat epicurei adipiscing ne vis.</h2>
                              <p class="lead white-text">Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                              <a class="main-button icon-button" href="#">Get Started!</a>
                         </div>

                    </div>
                    <!-- /row -->

               </div>
               <!-- /container -->

          </div>
          <!-- /Call To Action -->

          <!-- Why us -->
          <div id="why-us" class="section">

               <!-- container -->
               <div class="container">

                    <!-- row -->
                    <div class="row">
                         <div class="section-header text-center">
                              <h2>Why Edusite</h2>
                              <p class="lead">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
                         </div>

                         <!-- feature -->
                         <div class="col-md-4">
                              <div class="feature">
                                   <i class="feature-icon fa fa-flask"></i>
                                   <div class="feature-content">
                                        <h4>Online Courses</h4>
                                        <p>Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                                   </div>
                              </div>
                         </div>
                         <!-- /feature -->

                         <!-- feature -->
                         <div class="col-md-4">
                              <div class="feature">
                                   <i class="feature-icon fa fa-users"></i>
                                   <div class="feature-content">
                                        <h4>Expert Teachers</h4>
                                        <p>Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                                   </div>
                              </div>
                         </div>
                         <!-- /feature -->

                         <!-- feature -->
                         <div class="col-md-4">
                              <div class="feature">
                                   <i class="feature-icon fa fa-comments"></i>
                                   <div class="feature-content">
                                        <h4>Community</h4>
                                        <p>Ceteros fuisset mei no, soleat epicurei adipiscing ne vis. Et his suas veniam nominati.</p>
                                   </div>
                              </div>
                         </div>
                         <!-- /feature -->

                    </div>
                    <!-- /row -->

                    <hr class="section-hr">

                    <!-- row -->
                    <div class="row">

                         <div class="col-md-6">
                              <h3>Persius imperdiet incorrupte et qui, munere nusquam et nec.</h3>
                              <p class="lead">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
                              <p>No vel facete sententiae, quodsi dolores no quo, pri ex tamquam interesset necessitatibus. Te denique cotidieque delicatissimi sed. Eu doming epicurei duo. Sit ea perfecto deseruisse theophrastus. At sed malis hendrerit, elitr deseruisse in sit, sit ei facilisi mediocrem.</p>
                         </div>

                         <div class="col-md-5 col-md-offset-1">
                              <a class="about-video" href="#">
                                   <img src="<?php echo base_url('assets/img/about-video.jpg')?>" alt="">
                                   <i class="play-icon fa fa-play"></i>
                              </a>
                         </div>

                    </div>
                    <!-- /row -->

               </div>
               <!-- /container -->

          </div>
          <!-- /Why us -->

          <!-- Contact CTA -->
          <div id="contact-cta" class="section">

               <!-- Backgound Image -->
               <div class="bg-image bg-parallax overlay" style="background-image:url(<?php echo base_url('assets/img/cta2-background.jpg')?>)"></div>
               <!-- Backgound Image -->

               <!-- container -->
               <div class="container">

                    <!-- row -->
                    <div class="row">

                         <div class="col-md-8 col-md-offset-2 text-center">
                              <h2 class="white-text">Contact Us</h2>
                              <p class="lead white-text">Libris vivendo eloquentiam ex ius, nec id splendide abhorreant.</p>
                              <a class="main-button icon-button" href="#">Contact Us Now</a>
                         </div>

                    </div>
                    <!-- /row -->

               </div>
               <!-- /container -->

          </div>
          <!-- /Contact CTA -->

          <!-- Footer -->
          <footer id="footer" class="section">

               <!-- container -->
               <div class="container">

                    <!-- row -->
                    <div class="row">

                         <!-- footer logo -->
                         <div class="col-md-6">
                              <div class="footer-logo">
                                   <a class="logo" href="index.html">
                                        <img src="<?php echo base_url('assets/img/logo11.png')?>" alt="logo">
                                   </a>
                              </div>
                         </div>
                         <!-- footer logo -->

                         <!-- footer nav -->
                         <div class="col-md-6">
                              <ul class="footer-nav">
                              <li><a href="<?php echo base_url() ?>">Home</a></li>
                              <li><a href="<?php echo base_url('register_hub') ?>">Inagata Hub</a></li>
                              <li><a href="blog.html">Inagata Class</a></li>
                              <li><a href="<?php echo base_url('register_internship') ?>">Inagata Internship</a></li>
                              </ul>
                         </div>
                         <!-- /footer nav -->

                    </div>
                    <!-- /row -->

                    <!-- row -->
                    <div id="bottom-footer" class="row">

                         <!-- social -->
                         <div class="col-md-4 col-md-push-8">
                              <ul class="footer-social">
                                   <li><a href="https://www.facebook.com/inagata.technosmith" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                   <li><a href="https://twitter.com/inagatatechno" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                   <li><a href="https://plus.google.com/114674971997785219746/" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                                   <li><a href="https://www.linkedin.com/company/inagata-technosmith" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                              </ul>
                         </div>
                         <!-- /social -->

                         <!-- copyright -->
                         <div class="col-md-8 col-md-pull-4">
                              <div class="footer-copyright">
                                   <!-- <span>&copy; Copyright 2018. All Rights Reserved. | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com">Colorlib</a></span> -->
                              </div>
                         </div>
                         <!-- /copyright -->

                    </div>
                    <!-- row -->

               </div>
               <!-- /container -->

          </footer>
          <!-- /Footer -->

          <!-- preloader -->
          <!-- <div id='preloader'><div class='preloader'></div></div> -->
          <!-- /preloader -->


          <!-- jQuery Plugins -->
          <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
          <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
          <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
          <script type="text/javascript" src="<?php echo base_url('assets/js/main.js') ?>"></script>
          <script type="text/javascript" src="<?php echo base_url('assets/js/viewportchecker.js') ?>"></script>
          <script type="text/javascript">
          jQuery(document).ready(function() {
               jQuery('.feature').addClass("hidden").viewportChecker({
                   classToAdd: 'visible animated fadeIn', // Class to add to the elements when they are visible
                   offset: 100    
               });   
          });            
          </script>

     </body>
</html>
