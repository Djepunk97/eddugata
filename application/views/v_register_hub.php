<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Edugata Course an Edu</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:700%7CMontserrat:400,600" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>"/>

    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>"/>

    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>"/>
    <!-- Font Awesome Icon -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>
  <body>

    <!-- Header -->
    <header id="header">
      <div class="container">

        <div class="navbar-header">
          <!-- Logo -->
          <div class="navbar-brand">
            <a class="logo" href="<?php echo base_url()?>">
              <img src="<?php echo base_url('assets/img/logo.png')?>" alt="logo">
            </a>
          </div>
          <!-- /Logo -->

          <!-- Mobile toggle -->
          <button class="navbar-toggle">
            <span></span>
          </button>
          <!-- /Mobile toggle -->
        </div>

        <!-- Navigation -->
        <nav id="nav">
          <ul class="main-menu nav navbar-nav navbar-right">
            <li><a href="<?php echo base_url() ?>">Home</a></li>
            <li><a href="<?php echo base_url('register_hub') ?>">Inagata Hub</a></li>
            <li><a href="blog.html">Inagata Class</a></li>
            <li><a href="<?php echo base_url('register_internship') ?>">Inagata Internship</a></li>
          </ul>
        </nav>
        <!-- /Navigation -->

      </div>
    </header>
    <!-- /Header -->

    <!-- Hero-area -->
    <div class="hero-area section">

      <!-- Backgound Image -->
      <div class="bg-image bg-parallax overlay" style="background-image:url(<?php echo base_url('assets/img/page-background.jpg')?>)"></div>
      <!-- /Backgound Image -->

      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-center">
            <ul class="hero-area-tree">
              <li><a href="index.html">Home</a></li>
              <li>Inagata Internship</li>
            </ul>
            <h1 class="white-text">Get In Hub</h1>

          </div>
        </div>
      </div>

    </div>
    <!-- /Hero-area -->

    <!-- Contact -->
    <div id="contact" class="section">

      <!-- container -->
      <div class="container">

        <!-- row -->
        <div class="row">
          
          <!-- contact form -->
          <div class="col-md-5">
            <div class="contact-form">
              <h4>Registration Form</h4>
              <form method="post" action="<?php echo base_url('register_hub/register_user')?>">
                <!-- <input class="input" type="text" name="name" placeholder="Name">
                <input class="input" type="email" name="email" placeholder="Email">
                <input class="input" type="text" name="subject" placeholder="Subject">
                <textarea class="input" name="message" placeholder="Enter your Message"></textarea>
                <button class="main-button icon-button pull-right">Send Message</button> -->
                <div class="form-group">
                    <label class="control-label requiredField" for="nama">
                     Nama Lengkap
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Anda" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="nama">
                     Tempat Lahir
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="tmpt_lhr" name="tmpt_lhr" placeholder="Masukkan Tempat Lahir" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="tgl_lhr">
                     Tanggal Lahir
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="tgl_lhr" name="tgl_lhr" placeholder="hh/bb/tttt" type="date"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="name3">
                     Alamat
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat Anda" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField">
                     Jenis Kelamin
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <div class="">
                     <div class="radio">
                      <label class="radio">
                       <input name="jk" type="radio" value="Laki-laki"/>
                       Laki-laki
                      </label>
                     </div>
                     <div class="radio">
                      <label class="radio">
                       <input name="jk" type="radio" value="Perempuan"/>
                       Perempuan
                      </label>
                     </div>
                    </div>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="telp">
                     Telepon
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="telp" name="telp" placeholder="No. Telepon" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="email">
                     Email
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="email" name="email" placeholder="Email Anda" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="asal_sklh">
                     Asal Sekolah/Instansi
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="asal_sklh" name="asal_sklh" placeholder="Asal Sekolah" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="alamat_sklh">
                     Alamat Sekolah/Instansi
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="alamat_sklh" name="alamat_sklh" placeholder="Alamat Sekolah" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="jur_pro">
                     Jurusan/Prodi
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="jur_pro" name="jur_pro" placeholder="Jurusan / Prodi" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="skill">
                     Skill yang dikuasai beserta levelnya (1-10)
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <textarea class="form-control" cols="40" id="skill" name="skill" placeholder="Android --&gt; 8 IOS--&gt; 7 XML--&gt; 8" rows="10"></textarea>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="kontak_ins">
                     Kontak Instansi
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="kontak_ins" name="kontak_ins" type="text"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="minat">
                     Minat Magang
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <textarea class="form-control" cols="40" id="minat" name="minat" placeholder="Backend, Frontend, Android" rows="10"></textarea>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="mulai">
                     Mulai magang
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="mulai" name="mulai" placeholder="HH/BB/TTTT" type="date"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label requiredField" for="akhir">
                     Akhir Magang
                     <span class="asteriskField">
                      *
                     </span>
                    </label>
                    <input class="form-control" id="akhir" name="akhir" placeholder="HH/BB/TTTT" type="date"/>
                   </div>
                   <div class="form-group ">
                    <label class="control-label " for="darimana">
                     Darimana anda mengetahui inagata?
                    </label>
                    <input class="form-control" id="darimana" name="darimana" type="text"/>
                   </div>
                   <div class="form-group">
                    <div>
                     <button class="btn btn-custom " name="submit" type="submit">
                      Register
                     </button>
                    </div>
                   </div>
              </form>
            </div>
          </div>
          <!-- /contact form -->
          
          <!-- contact information -->
          <div class="col-md-6 col-md-offset-1">
            <h4>Contact Information</h4>
            <ul class="contact-details">
              <li><i class="fa fa-envelope"></i>contact@inagata.com</li>
              <li><i class="fa fa-phone"></i>+6282244488814</li>
              <li><i class="fa fa-map-marker"></i>Perumahan Griyashanta L-220 Malang, Jawa Timur Indonesia</li>
            </ul>

            <!-- contact map -->
            <div id="contact-map"></div>
            <!-- /contact map -->

          </div>
          <!-- contact information -->

        </div>
        <!-- /row -->

      </div>
      <!-- /container -->

    </div>
    <!-- /Contact -->

    <!-- Footer -->
    <footer id="footer" class="section">

      <!-- container -->
      <div class="container">

        <!-- row -->
        <div class="row">

          <!-- footer logo -->
          <div class="col-md-6">
            <div class="footer-logo">
              <a class="logo" href="index.html">
                <img src="<?php echo base_url('assets/img/logo.png')?>" alt="logo">
              </a>
            </div>
          </div>
          <!-- footer logo -->

          <!-- footer nav -->
          <div class="col-md-6">
            <ul class="footer-nav">
            <li><a href="<?php echo base_url() ?>">Home</a></li>
            <li><a href="<?php echo base_url('register_hub') ?>">Inagata Hub</a></li>
            <li><a href="blog.html">Inagata Class</a></li>
            <li><a href="<?php echo base_url('register_internship') ?>">Inagata Internship</a></li>
            </ul>
          </div>
          <!-- /footer nav -->

        </div>
        <!-- /row -->

        <!-- row -->
        <div id="bottom-footer" class="row">

          <!-- social -->
          <div class="col-md-4 col-md-push-8">
            <ul class="footer-social">
              <li><a href="https://www.facebook.com/inagata.technosmith" class="facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/inagatatechno" class="twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://plus.google.com/114674971997785219746/" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.linkedin.com/company/inagata-technosmith" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
          <!-- /social -->

          <!-- copyright -->
          <div class="col-md-8 col-md-pull-4">
            <div class="footer-copyright">
              <!-- <span>&copy; Copyright 2018. All Rights Reserved. | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com">Colorlib</a></span> -->
            </div>
          </div>
          <!-- /copyright -->

        </div>
        <!-- row -->

      </div>
      <!-- /container -->

    </footer>
    <!-- /Footer -->

    <!-- preloader -->
    <div id='preloader'><div class='preloader'></div></div>
    <!-- /preloader -->


    <!-- jQuery Plugins -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/google-map.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/main.js')?>"></script>

  </body>
</html>
