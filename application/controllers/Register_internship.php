<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_internship extends CI_Controller{

      public function __construct()
      {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->model('m_register');
      }

      public function index()
      {
        $this->load->view('v_register_internship');
      }

      public function register_user()
      {
        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('tmpt_lhr','Tempat Lahir','required');
        $this->form_validation->set_rules('tgl_lhr','Tanggal Lahir','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('jk','Jenis Kelamin','required');
        $this->form_validation->set_rules('telp','Telepon','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('asal_sklh','Asal Sekolah','required');
        $this->form_validation->set_rules('alamat_sklh','Alamat Sekolah','required');
        $this->form_validation->set_rules('jur_pro','Jurusan','required');
        $this->form_validation->set_rules('skill','Skill','required');
        $this->form_validation->set_rules('kontak_ins','Kontak Instansi','required');
        $this->form_validation->set_rules('minat','Minat','required');
        $this->form_validation->set_rules('mulai','Mulai Magang','required');
        $this->form_validation->set_rules('akhir','Akhir Magang','required');
        $this->form_validation->set_rules('darimana','Darimana');

        $tgl_lahir  = $this->input->post('tgl_lhr', TRUE);
        $mulai      = $this->input->post('mulai', TRUE);
        $akhir      = $this->input->post('akhir', TRUE);
        $email      = $this->input->post('email', TRUE);
        $nama_lkp   = $this->input->post('nama', TRUE);
        $tmpt_lhr   = $this->input->post('tmpt_lhr', TRUE);
        $alamat     = $this->input->post('alamat', TRUE);
        $telp       = $this->input->post('telp', TRUE);
        $jk         = $this->input->post('jk', TRUE);
        $asal_sklh  = $this->input->post('asal_sklh', TRUE);
        $alamat_sklh= $this->input->post('alamat_sklh', TRUE);
        $jurusan    = $this->input->post('jur_pro', TRUE);
        $skill      = $this->input->post('skill', TRUE);
        $kontak_ins = $this->input->post('kontak_ins', TRUE);
        $minat      = $this->input->post('minat', TRUE);
        $drmn       = $this->input->post('darimana', TRUE);
        $tgl_lhr_con= date('Y-m-d', strtotime($tgl_lahir));
        $tgl_mulai  = date('Y-m-d', strtotime($mulai));
        $tgl_akhir  = date('Y-m-d', strtotime($akhir));

        if ($this->form_validation->run() == FALSE)
        {
          $this->load->view('v_registration_internship');
        }else
        {
          $data = array(
            'nama_lkp' => $nama_lkp,
            'tmpt_lhr' => $tmpt_lhr,
            'tgl_lhr' => $tgl_lhr_con,
            'alamat' => $alamat,
            'jk' => $jk,
            'telp' => $telp,
            'email' => $email,
            'asal_sklh' => $asal_sklh,
            'alamat_sklh' => $alamat_sklh,
            'jurusan' => $jurusan,
            'skill' => $skill,
            'kontak_instansi' => $kontak_ins,
            'minat' => $minat,
            'mulai' => $tgl_mulai,
            'akhir' => $tgl_akhir,
            'drmn' => $drmn
         );

         if($this->m_register->insert_user_internship($data))
         {
            if($this->sendemail($email))
            {
              //print_r($email);
            }else
            {
                
            }
            $this->load->view('after_register');
         }else
         {
            redirect('register_internship');
         }
        
       }
    }

      function sendemail($email)
      {
        $this->form_validation->set_rules('nama','Nama','required');
        $this->form_validation->set_rules('tmpt_lhr','Tempat Lahir','required');
        $this->form_validation->set_rules('tgl_lhr','Tanggal Lahir','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('jk','Jenis Kelamin','required');
        $this->form_validation->set_rules('telp','Telepon','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('asal_sklh','Asal Sekolah','required');
        $this->form_validation->set_rules('alamat_sklh','Alamat Sekolah','required');
        $this->form_validation->set_rules('jur_pro','Jurusan','required');
        $this->form_validation->set_rules('skill','Skill','required');
        $this->form_validation->set_rules('kontak_ins','Kontak Instansi','required');
        $this->form_validation->set_rules('minat','Minat','required');
        $this->form_validation->set_rules('mulai','Mulai Magang','required');
        $this->form_validation->set_rules('akhir','Akhir Magang','required');
        $this->form_validation->set_rules('darimana','Darimana');
        
        if ($this->form_validation->run() == FALSE)
        {
          $this->load->view('v_register_internship');
        }else
        {
        $data = $this->input->post();
        $tgl_lhr_con= date('Y-m-d', strtotime($data['tgl_lhr']));
        $tgl_mulai  = date('Y-m-d', strtotime($data['mulai']));
        $tgl_akhir  = date('Y-m-d', strtotime($data['akhir']));

      $config = array(
      'protocol'    => 'smtp',
      'smtp_host'   => 'ssl://mail.inagata.com',
      'smtp_port'   =>  465,
      'smtp_user'   => 'contact@inagata.com',
      'smtp_pass'   => 'terserahkamu',
      'mailtype'    => 'html',
      'charset'     => 'iso-8859-1',
      'newline'     => "\r\n",
      'wordwrap'    => TRUE
      );
      //$this->load->library('email',$config);
      $this->email->initialize($config);
      $this->email->from($email,$data['nama']);
      $this->email->to('contact@inagata.com');
      $this->email->subject('Data Pendaftar Internship');
      $message = "
      <html>
        <head>
        </head>
        <body>
          <table>
            <tr>
              <td>Nama Lengkap</td>
              <td>".$data['nama']."</td>
            </tr>
            <tr>
              <td>Tempat Lahir</td>
              <td>".$data['tmpt_lhr']."</td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td>
              <td>".$tgl_lhr_con."</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>".$data['alamat']."</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>".$data['jk']."</td>
            </tr>
            <tr>
              <td>Telp</td>
              <td>".$data['telp']."</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>".$data['email']."</td>
            </tr>
            <tr>
              <td>Asal Sekolah/Instansi</td>
              <td>".$data['asal_sklh']."</td>
            </tr>
            <tr>
              <td>Alamat Sekolah/Instansi</td>
              <td>".$data['alamat_sklh']."</td>
            </tr>
            <tr>
              <td>Jurusan</td>
              <td>".$data['jur_pro']."</td>
            </tr>
            <tr>
              <td>Skill</td>
              <td>".$data['skill']."</td>
            </tr>
            <tr>
              <td>Kontak Sekolah/Instansi</td>
              <td>".$data['kontak_ins']."</td>
            </tr>
            <tr>
              <td>Minat</td>
              <td>".$data['minat']."</td>
            </tr>
            <tr>
              <td>Mulai</td>
              <td>".$tgl_mulai."</td>
            </tr>
            <tr>
              <td>Akhir</td>
              <td>".$tgl_akhir."</td>
            </tr>
            <tr>
              <td>Darimana Anda Tahu?</td>
              <td>".$data['darimana']."</td>
            </tr>
          </table>
        </body>
      </html>";
      $this->email->message($message);
      //$this->email->send();
      //echo $this->email->print_debugger();
    if(!$this->email->send()){
      show_error($this->email->print_debugger());
      echo "sesssssss";
    }else{
        print_r($email);
    }
    }
    }
}
?>
